package itis.quiz.spaceships;

import java.util.ArrayList;
import java.util.Collections;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        ArrayList<Integer> array = new ArrayList<>();
        for (Spaceship ship : ships) {
            array.add(ship.getFirePower());
        }
        Collections.sort(array);

        for (Spaceship ship : ships) {
            if (array.get(array.size() - 1) > 0) {
                if (ship.getFirePower() == array.get(array.size() - 1)) {
                    return ship;
                }
            }
        }
        return null;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        for (Spaceship ship : ships) {
            if (ship.getName().equals(name)) {
                return ship;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> ar = new ArrayList<>();
        for (Spaceship ship : ships) {
            if (ship.getCargoSpace() >= cargoSize) {
                ar.add(ship);
            }
        }
        if (ar.isEmpty()) {
            return null;
        }
        return ar;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> ar = new ArrayList<>();
        for (Spaceship ship : ships) {
            if (ship.getFirePower() == 0) {
                ar.add(ship);
            }
        }
        if (ar.isEmpty()) {
            return null;
        }
        return ar;
    }
}