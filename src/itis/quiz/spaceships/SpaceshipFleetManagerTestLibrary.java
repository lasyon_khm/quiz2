package itis.quiz.spaceships;

import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

public class SpaceshipFleetManagerTestLibrary {

    static SpaceshipFleetManager spaceshipFleetManager;
    static boolean flag;
    ArrayList<Spaceship> testList = new ArrayList<>();

    @BeforeAll
    static void beforeAll() {
        spaceshipFleetManager = new CommandCenter();
    }

    @AfterAll
    static void afterAll() {
        System.out.println("The end");
    }

    @BeforeEach
    void beforeEach() {
        testList.add(new Spaceship("Mark", 0, 31, 100));
        testList.add(new Spaceship("Foo", 120, 40, 100));
        testList.add(new Spaceship("Bob", 50, 100, 100));
        testList.add(new Spaceship("Tom", 110, 0, 100));
        flag = false;
    }

    @AfterEach
    void afterEach() {// параметр не подается вроде лист
        testList.clear();
    }

    @Test
    @DisplayName("Возвращает самый хорошо вооруженный корабль ")
    void mostPowerful_theOnlyExists() {//1m1t
        Spaceship result = spaceshipFleetManager.getMostPowerfulShip(testList);

        if (result != null && result.getFirePower() == 120) {
            flag = !flag;
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("Возвращает первый по списку самый хорошо вооруженный корабль ")
    void mostPowerful_fewExist() {//1m2t
        testList.add(new Spaceship("Normandy", 120, 30, 100));

        Spaceship result = spaceshipFleetManager.getMostPowerfulShip(testList);

        String testName = "Foo";
        if (result != null && result.getFirePower() == 120 && result.getName().equals(testName)) {
            flag = !flag;
        }
        assertTrue(flag);
    }


    @Test
    @DisplayName("Если самого хорошо вооруженного корабля нет, возвращает null")
    void mostPowerful_notExists() {//1m3t
        testList.remove(1);
        testList.remove(1);
        testList.remove(1);

        Spaceship result = spaceshipFleetManager.getMostPowerfulShip(testList);

        if (result == null) {
            flag = !flag;
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("Возвращает корабль с заданным именем")
    void byName_Exists() {// 2m1t
        Spaceship result = spaceshipFleetManager.getShipByName(testList, "Mark");

        if (result != null && result.getName().equals("Mark")) {
            flag = !flag;
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("Если корабля с заданным именем нет, возвращает null")
    void byName_notExists() {//2m2t
        Spaceship result = spaceshipFleetManager.getShipByName(testList, "Heritage");

        if (result == null) {
            flag = !flag;
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("Возвращает только корабли с достаточно большим грузовым трюмом")
    void byEnoughCargoSpace_Exists() {//3m1t
        ArrayList<Spaceship> result = spaceshipFleetManager.getAllShipsWithEnoughCargoSpace(testList, 35);

        if (result.size() % 2 == 0) {
            for (Spaceship ship : result) {
                if (result != null && ship.getCargoSpace() >= 35) {
                    flag = !flag;
                }
            }
            assertFalse(flag);
        } else {
            for (Spaceship ship : result) {
                if (result != null && ship.getCargoSpace() >= 35) {
                    flag = !flag;
                }
                assertTrue(flag);
            }
        }
    }

    @Test
    @DisplayName("Если кораблей c достаточно большим грузовым трюмом нет, возвращает пустой список.")
    void byEnoughCargoSpace_notExists() {//3m2t
        testList.remove(0);
        testList.remove(0);
        testList.remove(0);
        ArrayList<Spaceship> result = spaceshipFleetManager.getAllShipsWithEnoughCargoSpace(testList, 35);

        if (result == null) {
            flag = !flag;
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("Возвращает только мирные корабли")
    void civilianShips_Exists() {//4m1t
        ArrayList<Spaceship> result = spaceshipFleetManager.getAllCivilianShips(testList);

        for (Spaceship ship : result) {
            if (ship.getFirePower() == 0) {
                flag = !flag;
            }
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("Если мирных кораблей нет, возвращает пустой список.")
    void civilianShips_notExists() {//4m2t
        testList.remove(0);

        ArrayList<Spaceship> result = spaceshipFleetManager.getAllCivilianShips(testList);

        if (result == null) {
            flag = !flag;
        }
        assertTrue(flag);
    }
}