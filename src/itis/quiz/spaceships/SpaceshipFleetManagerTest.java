package itis.quiz.spaceships;

import java.util.ArrayList;

public class SpaceshipFleetManagerTest {

    SpaceshipFleetManager center;

    ArrayList<Spaceship> testList = new ArrayList<>();

    public static void main(String[] args) {

        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest(new CommandCenter());

        boolean test1 = spaceshipFleetManagerTest.getMostPowerful_isMostPowerful_returnShip();
        boolean test2 = spaceshipFleetManagerTest.getMostPowerful_ifSeveral_returnFirst();
        boolean test3 = spaceshipFleetManagerTest.getMostPowerful_ifNoSuchShip_returnNull();
        boolean test4 = spaceshipFleetManagerTest.getShipByName_shipExists_returnTargetShip();
        boolean test5 = spaceshipFleetManagerTest.getShipByName_shipNotExists_returnNull();
        boolean test6 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_shipsExit_returnShips();
        boolean test7 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_shipsNotExit_returnNull();
        boolean test8 = spaceshipFleetManagerTest.getAllCivilianShips_civilianShipsExist_returnShips();
        boolean test9 = spaceshipFleetManagerTest.getAllCivilianShips_civilianShipsNotExist_returnNull();

        if(test1){
            System.out.println("OK test1");
        }
        else{
            System.out.println("NO test1");

        }

        if(test2){
            System.out.println("OK test2");
        }
        else{
            System.out.println("NO test2");

        }

        if(test3){
            System.out.println("OK test3");
        }
        else{
            System.out.println("NO test3");

        }

        if (test4) {
            System.out.println("OK test4");
        } else {
            System.out.println("NO test4");
        }

        if (test5) {
            System.out.println("OK test5");
        } else {
            System.out.println("NO test5");
        }

        if (test6) {
            System.out.println("OK test6");
        } else {
            System.out.println("NO test6");
        }

        if (test7) {
            System.out.println("OK test7");
        } else {
            System.out.println("NO test7");
        }

        if (test8) {
            System.out.println("OK test8");
        } else {
            System.out.println("NO test8");
        }

        if (test9) {
            System.out.println("OK test9");
        } else {
            System.out.println("NO test9");
        }
        int points = 0;

        if(test1 && test2 && test3){
            points++;
        }

        if(test4 && test5){
            points++;
        }
        if (test6 && test7){
            points++;
        }
        if(test8 && test9){
            points++;
        }
        System.out.println("You earned " + points);
    }

    private boolean getMostPowerful_isMostPowerful_returnShip() {
        testList.add(new Spaceship("Foo", 10, 50, 20));
        testList.add(new Spaceship("Bar", 120, 50, 20));
        testList.add(new Spaceship("Mark", 110, 50, 20));

        Spaceship result = center.getMostPowerfulShip(testList);

        if(result != null && result.getFirePower() == 120){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private boolean getMostPowerful_ifSeveral_returnFirst(){

        testList.add(new Spaceship("Foo", 10, 50, 20));
        testList.add(new Spaceship("Bar", 120, 50, 20));
        testList.add(new Spaceship("Mark", 120, 50, 20));

        String testName = "Bar";
        Spaceship result = center.getMostPowerfulShip(testList);
        if(result != null && result.getFirePower() == 120 && result.getName().equals(testName)){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private boolean getMostPowerful_ifNoSuchShip_returnNull() {

        testList.add(new Spaceship("Foo", 0, 50, 20));
        testList.add(new Spaceship("Bar", 0, 50, 20));
        testList.add(new Spaceship("Mark", 0, 50, 20));

        Spaceship result = center.getMostPowerfulShip(testList);

        if(result == null){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private boolean getShipByName_shipExists_returnTargetShip() {

        testList.add(new Spaceship("Foo", 100, 50, 20));
        testList.add(new Spaceship("Bar", 100, 50, 20));
        testList.add(new Spaceship("Mark", 100, 50, 20));

        String testName = "Bar";
        Spaceship result = center.getShipByName(testList, testName);

        if (result != null && result.getName().equals(testName)) {
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private boolean getShipByName_shipNotExists_returnNull() {

        testList.add(new Spaceship("Foo", 100, 50, 20));
        testList.add(new Spaceship("Bar", 100, 50, 20));
        testList.add(new Spaceship("Mark", 100, 50, 20));

        String testName = "Mark 2";
        Spaceship result = center.getShipByName(testList, testName);

        if (result == null) {
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpace_shipsExit_returnShips() {

        testList.add(new Spaceship("Foo", 100, 50, 20));
        testList.add(new Spaceship("Bar", 100, 60, 20));
        testList.add(new Spaceship("Mark", 100, 70, 20));

        Integer testCargoSpace = 51;
        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testList, testCargoSpace);

        for (Spaceship ship : result) {
            if (result != null && ship.getCargoSpace() >= testCargoSpace) {
                testList.clear();
                return true;
            }
        }
        testList.clear();
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpace_shipsNotExit_returnNull() {

        testList.add(new Spaceship("Foo", 100, 28, 20));
        testList.add(new Spaceship("Bar", 100, 30, 20));
        testList.add(new Spaceship("Mark", 100, 40, 20));

        Integer testCargoSpace = 51;
        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testList, testCargoSpace);

        if (result == null) {
            testList.clear();
            return true;
        }

        testList.clear();
        return false;
    }

    private boolean getAllCivilianShips_civilianShipsExist_returnShips() {

        testList.add(new Spaceship("Foo", 0, 50, 20));
        testList.add(new Spaceship("Bar", 0, 60, 20));
        testList.add(new Spaceship("Mark", 100, 70, 20));

        ArrayList<Spaceship> result = center.getAllCivilianShips(testList);

        for (Spaceship ship : result) {
            if (result != null && ship.getFirePower() == 0) {
                testList.clear();
                return true;
            }
        }
        testList.clear();
        return false;
    }

    private boolean getAllCivilianShips_civilianShipsNotExist_returnNull() {

        testList.add(new Spaceship("Foo", 10, 50, 20));
        testList.add(new Spaceship("Bar", 20, 60, 20));
        testList.add(new Spaceship("Mark", 100, 70, 20));

        ArrayList<Spaceship> result = center.getAllCivilianShips(testList);

        if (result == null) {
            testList.clear();
            return true;
        }

        testList.clear();
        return false;
    }

    public SpaceshipFleetManagerTest(SpaceshipFleetManager center) {
        this.center = center;
    }
}
